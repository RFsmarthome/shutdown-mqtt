module schneiderlein.xyz/shutdown-mqtt

go 1.17

require (
	github.com/coreos/go-systemd v0.0.0-20191104093116-d3cd4ed1dbcf // indirect
	github.com/eclipse/paho.mqtt.golang v1.3.5 // indirect
	github.com/godbus/dbus v4.1.0+incompatible // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	golang.org/x/net v0.0.0-20211216030914-fe4d6282115f // indirect
)
