package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/coreos/go-systemd/login1"
	MQTT "github.com/eclipse/paho.mqtt.golang"
)

var topicSub string
var topicPub string

func onMessageReceive(client MQTT.Client, message MQTT.Message) {
	fmt.Printf("\n\nReceived on topic %s\nMessage: %s\n", message.Topic(), message.Payload())

	m := string(message.Payload())
	if m == "OFF" {
		token := client.Publish(topicPub, byte(0), true, "OFF")
		t, _ := time.ParseDuration("5s")
		if token.WaitTimeout(t) {
			fmt.Println("OKAY")

			con, _ := login1.New()
			con.PowerOff(false)
			con.Close()
		}
	}
}

func main() {
	signalChannel := make(chan os.Signal, 1)
	signal.Notify(signalChannel,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	hostname, _ := os.Hostname()
	server := flag.String("server", "tcp://mqtt.fritz.box:1883", "MQTT host")
	clientId := flag.String("clientid", "shutdown_"+hostname+strconv.Itoa(time.Now().Second()), "The MQTT client ID")
	pcId := flag.String("pcid", hostname, "The topic PC ID")
	flag.Parse()

	topicPub = "schnake/shutdown/" + *pcId + "/STATE"
	topicSub = "schnake/shutdown/" + *pcId + "/CMD"
	conOpts := MQTT.NewClientOptions().AddBroker(*server).SetClientID(*clientId).SetCleanSession(true)
	conOpts.OnConnect = func(c MQTT.Client) {
		if token := c.Subscribe(topicSub, byte(0), onMessageReceive); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}
	}
	conOpts.SetCleanSession(false)
	conOpts.SetWill(topicPub, "OFF", byte(0), true)

	client := MQTT.NewClient(conOpts)

	fmt.Printf("Connected to server %s\nSubscribe to topic %s\nReceive on topic %s\n", *server, topicSub, topicPub)

	timeoutConnect, _ := time.ParseDuration("15s")
	var conToken MQTT.Token
	for ok := true; ok; ok = conToken.WaitTimeout(timeoutConnect) && conToken.Error() != nil {
		fmt.Println("Try to connect")
		conToken = client.Connect()
		t, _ := time.ParseDuration("200ms")
		time.Sleep(t)
	}

	fmt.Println("Send ON")
	client.Publish(topicPub, byte(0), true, "ON")

	<-signalChannel

	fmt.Println("Catch signal!")
	fmt.Println("Send OFF")
	token := client.Publish(topicPub, byte(0), true, "OFF")
	t, _ := time.ParseDuration("5s")
	token.WaitTimeout(t)
}
